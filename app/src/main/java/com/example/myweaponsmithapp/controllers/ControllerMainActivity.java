package com.example.myweaponsmithapp.controllers;

import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.myweaponsmithapp.R;
import com.example.myweaponsmithapp.model.DataStorage;
import com.example.myweaponsmithapp.model.DbManager;
import com.example.myweaponsmithapp.model.entities.Weapon;
import com.example.myweaponsmithapp.model.entities.Weaponsmith;
import com.example.myweaponsmithapp.views.MainActivity;
import com.example.myweaponsmithapp.views.main_activity_tools.WeaponsAdapter;

import java.util.ArrayList;

public class ControllerMainActivity
{
    private MainActivity mainActivity;
    private DbManager db;

    private ArrayList<Weapon> weapons;

    public ControllerMainActivity(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;

        db = DbManager.getInstance(this.mainActivity.getApplicationContext());

        weapons = db.getTableWeapons().getAll();

        TextView textView = this.mainActivity.findViewById(R.id.textViewWeapon);
        Weaponsmith weaponsmith = (Weaponsmith) DataStorage.Get("weaponsmith");

        textView.setText("Welcome: "+weaponsmith.getName());
    }

    private void UpdateListViewWeapons(ArrayList<Weapon> weapons)
    {
        WeaponsAdapter adapter = new WeaponsAdapter(weapons, mainActivity.getApplicationContext());

        ListView listView = mainActivity.findViewById(R.id.listViewWeapons);

        listView.setAdapter(adapter);
    }

    public void UpdateListViewWeaponsWithoutFilter()
    {
        UpdateListViewWeapons(weapons);
    }

    public void UpdateListViewWeaponsWithFilter()
    {
        ArrayList<Weapon> filterWeapons = new ArrayList<>();

        EditText editText = mainActivity.findViewById(R.id.editTextRating);
        String ratingPart = editText.getText().toString();

        for (int i = 0; i < weapons.size(); i++)
        {
            if (weapons.get(i).getRating().startsWith(ratingPart)==true)
            {
                filterWeapons.add(weapons.get(i));
            }
        }

        UpdateListViewWeapons(filterWeapons);
    }

    public void ReloadWeapons()
    {
        weapons = db.getTableWeapons().getAll();
    }

    public void LogOut()
    {
        DataStorage.Delete("weaponsmith");
        mainActivity.finish();
    }
}
