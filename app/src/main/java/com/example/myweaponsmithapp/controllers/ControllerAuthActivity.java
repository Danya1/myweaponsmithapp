package com.example.myweaponsmithapp.controllers;

import android.content.Context;
import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myweaponsmithapp.R;
import com.example.myweaponsmithapp.model.DataStorage;
import com.example.myweaponsmithapp.model.DbManager;
import com.example.myweaponsmithapp.model.entities.Weaponsmith;
import com.example.myweaponsmithapp.views.AuthActivity;
import com.example.myweaponsmithapp.views.MainActivity;

public class ControllerAuthActivity
{
    private AuthActivity authActivity;
    private DbManager db;

    public ControllerAuthActivity(AuthActivity authActivity)
    {
        this.authActivity = authActivity;
        db = DbManager.getInstance(this.authActivity.getApplicationContext());
    }

    public void Authorize()
    {
        EditText editTextLogin, editTextPassword;
        Context context;

        editTextLogin = authActivity.findViewById(R.id.editTextLogin);
        editTextPassword = authActivity.findViewById(R.id.editTextPassword);

        context = authActivity.getApplicationContext();

        String login = editTextLogin.getText().toString();
        String password = editTextPassword.getText().toString();

        if(login.equals("")==true)
        {
            Toast.makeText(context, "Error. Login is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if(password.equals("")==true)
        {
            Toast.makeText(context, "Error. Password is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        Weaponsmith weaponsmith = db.getTableWeaponsmiths().getByLoginAndPassword(login, password);

        if (weaponsmith==null)
        {
            Toast.makeText(context, "Error. Login or Password is wrong", Toast.LENGTH_SHORT).show();
        }
        else
        {
            editTextLogin.getText().clear();
            editTextPassword.getText().clear();

            DataStorage.Add("weaponsmith", weaponsmith);

            Intent intent = new Intent(context, MainActivity.class);
            authActivity.startActivity(intent);
        }
    }

    public void Exit()
    {
        authActivity.finish();
    }
}
