package com.example.myweaponsmithapp.views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myweaponsmithapp.R;
import com.example.myweaponsmithapp.controllers.ControllerMainActivity;

public class MainActivity extends AppCompatActivity {

    private ControllerMainActivity controller;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        controller = new ControllerMainActivity(this);
        controller.UpdateListViewWeaponsWithoutFilter();

        EditText editTextRating = findViewById(R.id.editTextRating);
        editTextRating.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                controller.UpdateListViewWeaponsWithFilter();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void onButtonReloadWeaponsClick(View view)
    {
        controller.ReloadWeapons();
        controller.UpdateListViewWeaponsWithoutFilter();

        Toast.makeText(getApplicationContext(), "Weapons successful reloaded", Toast.LENGTH_SHORT).show();
    }

    public void onButtonLogOutClick(View view)
    {
        controller.LogOut();
    }
}
