package com.example.myweaponsmithapp.views.main_activity_tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.myweaponsmithapp.model.entities.Weapon;

import java.util.ArrayList;

public class WeaponsAdapter extends BaseAdapter
{
    private ArrayList<Weapon> weapons;

    Context context;
    LayoutInflater layoutInflater;

    public WeaponsAdapter(ArrayList<Weapon> weapons, Context context)
    {
        this.context = context;
        this.weapons = weapons;

        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return weapons.size();
    }

    @Override
    public Object getItem(int i) {
        return weapons.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        if(v == null)
        {
            v = layoutInflater.inflate(android.R.layout.simple_list_item_1, viewGroup, false);
        }

        Weapon weapon = weapons.get(i);

        TextView textView = v.findViewById(android.R.id.text1);

        String str = String.format("Name: %s\n Material: %s\n Rating: %s", weapon.getName(), weapon.getMaterial(), weapon.getRating());

        textView.setText(str);

        return v;
    }
}
