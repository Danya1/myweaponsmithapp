package com.example.myweaponsmithapp.views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.example.myweaponsmithapp.R;
import com.example.myweaponsmithapp.controllers.ControllerAuthActivity;

public class AuthActivity extends AppCompatActivity {

    private ControllerAuthActivity controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        controller = new ControllerAuthActivity(this);
    }

    public void onButtonAuthorizeClick(View view)
    {
        controller.Authorize();
    }

    public void onButtonExitClick(View view)
    {
        controller.Exit();
    }
}