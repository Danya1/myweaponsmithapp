package com.example.myweaponsmithapp.model.tools;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper
{

    public DbHelper(Context context)
    {
        super(context, "app.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS \"weaponsmiths\" (\n" +
                "\t\"id\"\tINTEGER NOT NULL,\n" +
                "\t\"name\"\tTEXT NOT NULL,\n" +
                "\t\"login\"\tTEXT NOT NULL,\n" +
                "\t\"password\"\tTEXT NOT NULL,\n" +
                "\tPRIMARY KEY(\"id\" AUTOINCREMENT)\n" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS \"weapons\" (\n" +
                "\t\"id\"\tINTEGER NOT NULL,\n" +
                "\t\"name\"\tTEXT NOT NULL,\n" +
                "\t\"material\"\tTEXT NOT NULL,\n" +
                "\t\"rating\"\tTEXT NOT NULL,\n" +
                "\tPRIMARY KEY(\"id\" AUTOINCREMENT)\n" +
                ");");

        db.execSQL("DELETE FROM weaponsmiths");
        db.execSQL("DELETE FROM weapons");

        db.execSQL("INSERT INTO \"weaponsmiths\" (\"id\",\"name\",\"login\",\"password\") VALUES (1,'Александр','Alexander','135'),\n" +
                " (2,'Анатолий','Anatoliy','246');");

        db.execSQL("INSERT INTO \"weapons\" (\"id\",\"name\",\"material\",\"rating\") VALUES (1,'Когти','Аллюминий','6'),\n" +
                " (2,'Мечи','Железо','4'),\n" +
                " (3,'Тесаки','Титан','9'),\n" +
                " (4,'Катана','Тантал','8');");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
