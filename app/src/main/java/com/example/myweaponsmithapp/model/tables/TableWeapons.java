package com.example.myweaponsmithapp.model.tables;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.myweaponsmithapp.model.entities.Weapon;
import com.example.myweaponsmithapp.model.tools.DbHelper;

import java.util.ArrayList;

public class TableWeapons
{
    private DbHelper dbHelper;

    public TableWeapons(DbHelper dbHelper)
    {
        this.dbHelper = dbHelper;
    }

    public ArrayList<Weapon> getAll()
    {
        ArrayList<Weapon> weapons = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `weapons`";

        Cursor cursor = db.rawQuery(sqlCommand, null);

        while (cursor.moveToNext()==true)
        {
            Weapon weapon = new Weapon(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3)
            );

            weapons.add(weapon);
        }

        cursor.close();

        dbHelper.close();

        return weapons;
    }
}
