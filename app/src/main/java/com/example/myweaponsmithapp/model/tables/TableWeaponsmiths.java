package com.example.myweaponsmithapp.model.tables;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.myweaponsmithapp.model.entities.Weaponsmith;
import com.example.myweaponsmithapp.model.tools.DbHelper;

public class TableWeaponsmiths
{
    private DbHelper dbHelper;

    public TableWeaponsmiths (DbHelper dbHelper)
    {
        this.dbHelper = dbHelper;
    }

    public Weaponsmith getByLoginAndPassword(String login, String password)
    {
        Weaponsmith weaponsmith = null;

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = String.format("SELECT * FROM `weaponsmiths` WHERE `login`='%s' AND `password`='%s'", login, password);

        Cursor cursor = db.rawQuery(sqlCommand, null);
        if (cursor.moveToFirst()==true)
        {
            weaponsmith = new Weaponsmith(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3)
            );
        }

        dbHelper.close();

        return weaponsmith;
    }
}
