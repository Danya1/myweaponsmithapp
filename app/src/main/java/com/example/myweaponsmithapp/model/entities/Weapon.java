package com.example.myweaponsmithapp.model.entities;

public class Weapon
{
    private int id;
    private String name;
    private String material;
    private String rating;

    public Weapon(int id, String name, String material, String rating)
    {
        this.id = id;
        this.name = name;
        this.material = material;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getMaterial() {
        return material;
    }

    public String getRating() {
        return rating;
    }
}
