package com.example.myweaponsmithapp.model;

import android.content.Context;

import com.example.myweaponsmithapp.model.tables.TableWeapons;
import com.example.myweaponsmithapp.model.tables.TableWeaponsmiths;
import com.example.myweaponsmithapp.model.tools.DbHelper;

public class DbManager
{
    private static DbManager instance = null;

    public static DbManager getInstance(Context context)
    {
        if (instance==null)
        {
            instance = new DbManager(context);
        }
        return instance;
    }

    private TableWeaponsmiths tableWeaponsmiths;
    private TableWeapons tableWeapons;

    private DbManager(Context context)
    {
        DbHelper dbHelper = new DbHelper(context);

        tableWeaponsmiths = new TableWeaponsmiths(dbHelper);
        tableWeapons = new TableWeapons(dbHelper);
    }

    public TableWeaponsmiths getTableWeaponsmiths() {
        return tableWeaponsmiths;
    }

    public TableWeapons getTableWeapons() {
        return tableWeapons;
    }
}
